# Commerce ShipStation

This module provides integration between
ShipStation (http://www.shipstation.com) and
Drupal Commerce for purposes of order fulfillment. ShipStation integration is
provided
via the ShipStation Custom Store
service (https://help.shipstation.com/hc/en-us/articles/360025856192-Custom-Store-Development-Guide).

The module supports the ability for ShipStation to retrieve orders from Drupal
Commerce
based on order status. It also allows ShipStation to notify Drupal Commerce of
orders
that have shipped.

## REQUIREMENTS

This module requires the following modules:

- [Commerce](https://drupal.org/project/commerce)
- [Commerce Shipping](https://drupal.org/project/commerce_shipping)

In order to use this module, you must have a ShipStation account.

## INSTALLATION

Install as you would normally install a contributed Drupal module. Visit
https://www.drupal.org/docs/extending-drupal/installing-modules for
further information.

## Configuration

1. In Drupal, configure the ShipStation connection at
   /admin/commerce/config/shipstation.
  1. Create a username and password separate from your ShipStation login, save
     these for the next step.
  2. Set the other config as necessary.
2. In ShipStation, add a "Custom Store" selling channel
  1. URL to Custom XML page is https://[your.domain.name]
     /shipstation/api-endpoint
  2. Use the username and password you created in the previous step.
  3. Make sure to set ShipStation's status codes to match the order workflow
     used by Drupal. It is recommended that you use "Fulfillment, with
     validation" and set the states as:
    1. Awaiting Payment Status: `validation`
    2. Awaiting Shipment Status: `fulfillment`
    3. Shipped Status: `complete`
    4. Cancelled Status: `canceled` *(note the spelling difference)*
    5. On-Hold Status: *null*
  4. If using a custom order workflow, make sure that there is a `fulfill`
     transition. This is the transition ShipStation will call after an order is
     shipped.
  5. Hit connect and Custom Store should connect.
3. That's it! ShipStation will automatically make export requests on the
   endpoint and import orders. It will also mark orders and shipments complete,
   and save a tracking number on the shipment.

## KNOWN ISSUES AND LIMITATIONS

[Issue Tracker](https://www.drupal.org/project/issues/commerce_shipstation?version=2.x)

* implement support for importing coupons and discounts
* implement support for importing custom order notes
* implement support for importing sales tax
