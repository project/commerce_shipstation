<?php

namespace Drupal\commerce_shipstation\Access;

use Drupal\commerce_shipstation\ShipStation;
use Drupal\Core\Access\AccessResult;
use Drupal\Core\Routing\Access\AccessInterface;
use Drupal\Core\Session\AccountProxy;

/**
 * Provide an access controller for the ShipStation endpoint.
 */
class ShipStationAccess implements AccessInterface {

  /**
   * The ShipStation service.
   *
   * @var \Drupal\commerce_shipstation\ShipStation
   *   The service.
   */
  private $shipStation;

  /**
   * Construct the access checker.
   *
   * @param \Drupal\commerce_shipstation\ShipStation $shipstation_service
   *   The service.
   */
  public function __construct(ShipStation $shipstation_service) {
    $this->shipStation = $shipstation_service;
  }

  /**
   * Check access.
   *
   * @param \Drupal\Core\Session\AccountProxy $account
   *   The current account.
   *
   * @return \Drupal\Core\Access\AccessResult|\Drupal\Core\Access\AccessResultAllowed
   *   Access check result.
   */
  public function access(AccountProxy $account) {
    if (!$account->isAnonymous() && $account->hasPermission('view any commerce_order')) {
      return AccessResult::allowed();
    }

    return $this->shipStation->access();
  }

}
