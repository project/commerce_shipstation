<?php

namespace Drupal\commerce_shipstation\Controller;

use Drupal\commerce_shipstation\ShipStation;
use Drupal\Core\Controller\ControllerBase;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Symfony\Component\HttpKernel\Exception\ServiceUnavailableHttpException;

/**
 * The shipstation endpoint controller.
 */
class ShipStationEndpointController extends ControllerBase {

  /**
   * ShipStation Configuration.
   *
   * @var \Drupal\Core\Config\ImmutableConfig
   */
  protected $shipStationConfig;

  /**
   * ShipStation Service.
   *
   * @var \Drupal\commerce_shipstation\ShipStation
   */
  protected $shipStation;

  /**
   * The shipstation logger.
   *
   * @var \Psr\Log\LoggerInterface
   */
  protected $logger;

  /**
   * The page cache disabling policy.
   *
   * @var \Drupal\Core\PageCache\ResponsePolicy\KillSwitch
   */
  protected $pageCacheKillSwitch;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    $instance = parent::create($container);
    $instance->shipStation = $container->get('commerce_shipstation.shipstation_service');
    $instance->configFactory = $container->get('config.factory');
    $instance->shipStationConfig = $instance->configFactory->get('commerce_shipstation.shipstation_config');
    /** @var \Drupal\Core\Logger\LoggerChannelFactoryInterface $logger_factory */
    $logger_factory = $container->get('logger.factory');
    $instance->setLoggerFactory($logger_factory);
    $instance->logger = $logger_factory->get('commerce_shipstation');
    $instance->pageCacheKillSwitch = $container->get('page_cache_kill_switch');
    /** @var \Drupal\Core\Messenger\MessengerInterface $messenger */
    $messenger = $container->get('messenger');
    $instance->setMessenger($messenger);
    return $instance;
  }

  /**
   * Establish a service endpoint for shipstation to communicate with.
   *
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   The request.
   *
   * @return \Symfony\Component\HttpFoundation\Response
   *   The response.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public function shipStationEndpointRequest(Request $request): Response {
    $this->pageCacheKillSwitch->trigger();
    if (empty($this->shipStationConfig->get())) {
      throw new ServiceUnavailableHttpException(60, 'Integration is not configured.');
    }
    $logging = $this->shipStationConfig->get('commerce_shipstation_logging');
    // ShipStation starts at 1, Drupal at 0.
    $request->query->set('page', (int) $request->query->get('page') - 1);

    // Log each request to the endpoint if logging is enabled.
    if ($logging) {
      $request_vars = $request->query->all();
      // Obfuscate the sensitive data before logging the request.
      $request_vars['SS-UserName'] = '******';
      $request_vars['SS-Password'] = '******';
      // $request_vars['auth_key'] = '*****';
      $this->logger->info('ShipStation request: @get', ['@get' => \json_encode($request_vars)]);
    }

    // Run the call based on the action it defines.
    $response = new Response();
    switch ($request->query->get('action')) {
      case ShipStation::EXPORT_ACTION:
        $start_date = $request->query->get('start_date') ?: '-1 day';
        $end_date = $request->query->get('end_date') ?: 'now';
        $page = $request->query->get('page') ?: 0;
        $xml = $this->shipStation->exportOrders($start_date, $end_date, $page);

        $response->headers->set('Content-type', 'application/xml');
        $response->setContent($xml);
        return $response;

      case ShipStation::SHIPNOTIFY_ACTION:
        $order_number = $request->query->get('order_number');
        $tracking_number = $request->query->get('tracking_number');
        $carrier = $request->query->get('carrier');
        $ship_date = $request->query->get('ship_date');
        $msg = $this->shipStation->requestShipNotify($order_number, $tracking_number, $carrier, $ship_date);
        $response->setContent($msg);
        return $response;

      default:
        $this->messenger()->addError($this->t('The ShipStation request action is invalid'));
        $this->logger->error('Invalid request action received from ShipStation. Enable or check request logging for more information');
        throw new AccessDeniedHttpException();
    }
  }

}
