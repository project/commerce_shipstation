<?php

namespace Drupal\commerce_shipstation\Event;

use Drupal\commerce_order\Entity\OrderInterface;
use Drupal\Component\EventDispatcher\Event;

/**
 * The shipstation order export event.
 */
class ShipStationOrderExportedEvent extends Event {
  /**
   * Commerce Order Entity.
   *
   * @var \Drupal\commerce_order\Entity\OrderInterface
   *   The order.
   */
  protected $order;

  /**
   * Constructs an ordered exported event object.
   *
   * @param \Drupal\commerce_order\Entity\OrderInterface $order
   *   The order.
   */
  public function __construct(OrderInterface $order) {
    $this->order = $order;
  }

  /**
   * Get the exported order.
   *
   * @return \Drupal\commerce_order\Entity\OrderInterface
   *   The order.
   */
  public function getOrder(): OrderInterface {
    return $this->order;
  }

}
