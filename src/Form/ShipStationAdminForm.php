<?php

namespace Drupal\commerce_shipstation\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\field\Entity\FieldConfig;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * ShipStation Admin Form.
 *
 * @package Drupal\commerce_shipstation\Form
 */
class ShipStationAdminForm extends ConfigFormBase {

  /**
   * The workflow manager.
   *
   * @var \Drupal\state_machine\WorkflowManagerInterface
   */
  protected $workflowManager;

  /**
   * The Shipping Method Manager.
   *
   * @var \Drupal\commerce_shipping\ShippingMethodManager
   */
  protected $shippingMethodManager;

  /**
   * The entity field manager service.
   *
   * @var \Drupal\Core\Entity\EntityFieldManagerInterface
   */
  protected $entityFieldManager;

  /**
   * The entity bundle info.
   *
   * @var \Drupal\Core\Entity\EntityTypeBundleInfoInterface
   */
  protected $entityTypeBundleInfo;

  /**
   * {@inheritDoc}
   */
  public static function create(ContainerInterface $container) {
    $instance = parent::create($container);
    $instance->workflowManager = $container->get('plugin.manager.workflow');
    $instance->shippingMethodManager = $container->get('plugin.manager.commerce_shipping_method');
    $instance->entityFieldManager = $container->get('entity_field.manager');
    $instance->entityTypeBundleInfo = $container->get('entity_type.bundle.info');
    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'commerce_shipstation.shipstation_config',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'shipstation_admin_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    // Get shipstation configuration.
    $ss_config = $this->config('commerce_shipstation.shipstation_config');

    // Get list of order states per defined workflow.
    $workflows = $this->workflowManager->getGroupedLabels('commerce_order');
    $states = [];
    foreach ($workflows['Order'] as $workflow_id => $workflow_value) {
      $workflow = $this->workflowManager->getDefinition($workflow_id);
      foreach ($workflow['states'] as $state_id => $state_value) {
        if (!isset($states[$state_id])) {
          $states[$state_id] = $state_value['label'];
        }
      }
    }

    // @todo Do we need to Break it down into Services?
    // Get Shipping Methods.
    $shipping_methods = $this->shippingMethodManager->getDefinitions();
    if (empty($shipping_methods)) {
      $form['commerce_shipstation_error_message'] = [
        '#markup' => $this->t("You'll need at least one shipping method module enabled, e.g. Commerce Flatrate shipping."),
      ];

      return $form;
    }

    $shipping_method_options = [];
    foreach ($shipping_methods as $key => $value) {
      $shipping_method_options[$key] = (string) $value['label'];
    }

    $profile_field_options = $this->loadFieldOptions('profile', 'customer');

    // ShipStation username.
    $form['commerce_shipstation_username'] = [
      '#type' => 'textfield',
      '#title' => $this->t('ShipStation Custom Store Username'),
      '#required' => TRUE,
      '#default_value' => $ss_config->get('commerce_shipstation_username'),
      '#description' => $this->t('Create a username for request authentication. This is NOT your ShipStation account username.'),
    ];

    // ShipStation password.
    $form['commerce_shipstation_password'] = [
      '#type' => 'password',
      '#title' => $this->t('ShipStation Custom Store Password'),
      '#required' => empty($ss_config->get('commerce_shipstation_password')),
      '#default_value' => $ss_config->get('commerce_shipstation_password'),
      '#attributes' => ['autocomplete' => 'off'],
      '#description' => $this->t('Create a password for request authentication. This is NOT your ShipStation account password.'),
    ];

    // ShipStation logging.
    $form['commerce_shipstation_logging'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Log requests to ShipStation'),
      '#description' => $this->t('If this is set, all API requests to ShipStation will be logged to Drupal watchdog.'),
      '#default_value' => $ss_config->get('commerce_shipstation_logging'),
    ];

    // ShipStation reload.
    $form['commerce_shipstation_reload'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Reload all orders to ShipStation'),
      '#description' => $this->t('If this is set, on API endpoint request all orders will be returned.'),
      '#default_value' => $ss_config->get('commerce_shipstation_reload'),
    ];

    // ShipStation alternate authentication.
    $form['commerce_shipstation_alternate_auth'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Alternate Authentication'),
      '#description' => $this->t('Use this field if your web server uses CGI to run PHP.'),
      '#default_value' => $ss_config->get('commerce_shipstation_alternate_auth'),
    ];

    // ShipStation export paging.
    $form['commerce_shipstation_export_paging'] = [
      '#type' => 'select',
      '#title' => $this->t('Number of Records to Export per Page'),
      '#description' => $this->t('Sets the number of orders to send to ShipStation at a time. Change this setting if you experience import timeouts.'),
      '#options' => [20 => 20, 50 => 50, 75 => 75, 100 => 100, 150 => 150],
      '#default_value' => $ss_config->get('commerce_shipstation_export_paging'),
    ];

    // Select phone number field.
    $form['commerce_shipstation_billing_phone_number_field'] = [
      '#type' => 'select',
      '#title' => $this->t('Field for billing phone number'),
      '#required' => FALSE,
      '#description' => $this->t('Select the field you are using for phone numbers in order data here.'),
      '#options' => $profile_field_options,
      '#default_value' => $ss_config->get('commerce_shipstation_billing_phone_number_field'),
    ];

    // Select phone number field.
    $form['commerce_shipstation_shipping_phone_number_field'] = [
      '#type' => 'select',
      '#title' => $this->t('Field for shipping phone number'),
      '#required' => FALSE,
      '#description' => $this->t('Select the field you are using for phone numbers in order data here.'),
      '#options' => $profile_field_options,
      '#default_value' => $ss_config->get('commerce_shipstation_shipping_phone_number_field'),
    ];

    // Order notes to import.
    $form['commerce_shipstation_order_notes_field'] = [
      '#type' => 'select',
      '#title' => $this->t('Field used for admin-facing order notes'),
      '#required' => FALSE,
      '#description' => $this->t('Choose a field you use for admin order notes (attached to Commerce Order entity).'),
      '#options' => $this->loadFieldOptions('commerce_order'),
      '#default_value' => $ss_config->get('commerce_shipstation_order_notes_field'),
    ];

    // Customer notes to import.
    $form['commerce_shipstation_customer_notes_field'] = [
      '#type' => 'select',
      '#title' => $this->t('Field used for customer-facing order notes'),
      '#required' => FALSE,
      '#description' => $this->t('Choose a field you use for customer shipping notes (attached to Commerce Order - Shipping Information entity).'),
      '#options' => $profile_field_options,
      '#default_value' => $ss_config->get('commerce_shipstation_customer_notes_field'),
    ];

    // Product images to import.
    $product_fields = $this->loadFieldOptions('commerce_product');
    unset($product_fields['commerce_product.variations']);
    $variation_fields = $this->loadFieldOptions('commerce_product_variation');
    $form['commerce_shipstation_product_images_field'] = [
      '#type' => 'select',
      '#title' => $this->t('Field used for product images'),
      '#required' => FALSE,
      '#description' => $this->t('Choose a field you use for product images.'),
      '#options' => array_merge($product_fields, $variation_fields),
      '#default_value' => $ss_config->get('commerce_shipstation_product_images_field'),
    ];

    // ShipStation order export status.
    $form['commerce_shipstation_export_status'] = [
      '#type' => 'select',
      '#title' => $this->t('Order Status to Export into ShipStation'),
      '#required' => TRUE,
      '#options' => $states,
      '#default_value' => $ss_config->get('commerce_shipstation_export_status'),
      '#multiple' => TRUE,
    ];

    // ShipStation available shipping methods.
    $form['commerce_shipstation_exposed_shipping_methods'] = [
      '#type' => 'checkboxes',
      '#title' => $this->t('Shipping Methods Available to ShipStation'),
      '#required' => TRUE,
      // May need to be drupal_map_assoc.
      '#options' => $shipping_method_options,
      '#default_value' => $ss_config->get('commerce_shipstation_exposed_shipping_methods') ?? [],
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);

    $config = $this->config('commerce_shipstation.shipstation_config');
    // $config->delete();
    $values = $form_state->cleanValues()->getValues();
    unset($values['actions']);
    // @todo Process password field.
    foreach ($values as $key => $value) {
      if ($key === 'commerce_shipstation_password' && !empty($value)) {
        $config->set($key, $value);
      }
      elseif ($key !== 'commerce_shipstation_password' && $config->get($key) !== $value) {
        $config->set($key, $value);
      }
    }
    $config->save();
  }

  /**
   * Builds a list of all fields available on the site.
   *
   * @param string $entity_type
   *   Entity Type.
   * @param string|null $bundle
   *   Entity bundle.
   * @param array $field_types
   *   Restrict options to certain types.
   *
   * @return array
   *   A list of options
   */
  private function loadFieldOptions(string $entity_type, ?string $bundle = NULL, array $field_types = []): array {
    $bundles = $this->entityTypeBundleInfo->getBundleInfo($entity_type);
    $fields = [];
    if ($bundle) {
      $fields = $this->entityFieldManager->getFieldDefinitions($entity_type, $bundle);
    }
    // Loop through each bundle and build the instance list.
    else {
      foreach ($bundles as $bundle_id => $bundle_value) {
        $instance = $this->entityFieldManager->getFieldDefinitions($entity_type, $bundle_id);
        foreach ($instance as $field_id => $field_value) {
          if (!isset($fields[$field_id])) {
            $fields[$field_id] = $field_value;
          }
        }
      }
    }

    $options = ['none' => $this->t('None')];
    if (!empty($fields)) {
      /** @var \Drupal\Core\Field\FieldDefinitionInterface $field */
      foreach ($fields as $field) {
        // Load Config Fields only.
        if ($field instanceof FieldConfig) {
          $field_type = $field->getType();
          // Check field type and limit to provided types.
          if (empty($field_types) || in_array($field_type, $field_types, TRUE)) {
            $field_name = (string) $field->getLabel();
            $bundle_name = $field->getTargetEntityTypeId();

            $options[$entity_type . '.' . $field->getName()] = $this->t('@bundle: @field', [
              '@bundle' => $bundle_name,
              '@field' => $field_name,
            ]);
          }
        }
      }
    }

    asort($options);
    return $options;
  }

}
