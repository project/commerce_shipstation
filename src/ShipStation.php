<?php

namespace Drupal\commerce_shipstation;

use Drupal\commerce_order\Entity\OrderItem;
use Drupal\commerce_shipstation\Event\ShipStationEvents;
use Drupal\commerce_shipstation\Event\ShipStationOrderExportedEvent;
use Drupal\Core\Access\AccessResult;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Datetime\DateFormatterInterface;
use Drupal\Core\Entity\EntityStorageException;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\Core\Url;
use Drupal\file\FileInterface;
use Psr\Log\LoggerInterface;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Contracts\EventDispatcher\EventDispatcherInterface;

/**
 * Manage ShipStation API services.
 *
 * @package Drupal\commerce_shipstation
 */
class ShipStation {

  use StringTranslationTrait;


  /**
   * Defines the shipstation format for date and time.
   */
  public const DATE_FORMAT = 'm/d/y H:m A';

  public const EXPORT_ACTION = 'export';

  public const SHIPNOTIFY_ACTION = 'shipnotify';

  /**
   * ShipStation configuration.
   *
   * @var \Drupal\Core\Config\ImmutableConfig
   */
  protected $shipStationConfig;

  /**
   * The entity_type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The event dispatcher service.
   *
   * @var \Symfony\Contracts\EventDispatcher\EventDispatcherInterface
   */
  protected $eventDispatcher;

  /**
   * The request made to the endpoint.
   *
   * @var \Symfony\Component\HttpFoundation\Request
   */
  protected $request;

  /**
   * The messenger service.
   *
   * @var \Drupal\Core\Messenger\MessengerInterface
   */
  protected $messenger;

  /**
   * The shipstation log service.
   *
   * @var \Psr\Log\LoggerInterface
   */
  protected $logger;

  /**
   * The module handler service.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected $moduleHandler;

  /**
   * The date formatter service.
   *
   * @var \Drupal\Core\Datetime\DateFormatterInterface
   */
  protected $dateFormatter;

  /**
   * The system's date configuration.
   *
   * @var \Drupal\Core\Config\ImmutableConfig
   */
  protected $systemDateConfig;

  /**
   * Constructs a new ShipStation Service.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The configuration.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   Entity type manager.
   * @param \Symfony\Contracts\EventDispatcher\EventDispatcherInterface $event_dispatcher
   *   Event dispatcher.
   * @param \Symfony\Component\HttpFoundation\RequestStack $request_stack
   *   Request stack.
   * @param \Drupal\Core\Messenger\MessengerInterface $messenger
   *   The messenger service.
   * @param \Psr\Log\LoggerInterface $logger
   *   The logger service.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler service.
   * @param \Drupal\Core\Datetime\DateFormatterInterface $date_formatter
   *   The date formatter service.
   */
  public function __construct(ConfigFactoryInterface $config_factory, EntityTypeManagerInterface $entity_type_manager, EventDispatcherInterface $event_dispatcher, RequestStack $request_stack, MessengerInterface $messenger, LoggerInterface $logger, ModuleHandlerInterface $module_handler, DateFormatterInterface $date_formatter) {
    $this->shipStationConfig = $config_factory->get('commerce_shipstation.shipstation_config');
    $this->systemDateConfig = $config_factory->get('system.date');
    $this->entityTypeManager = $entity_type_manager;
    $this->eventDispatcher = $event_dispatcher;
    $this->request = $request_stack->getCurrentRequest();
    $this->messenger = $messenger;
    $this->logger = $logger;
    $this->moduleHandler = $module_handler;
    $this->dateFormatter = $date_formatter;
  }

  /**
   * Authorizes a ShipStation Request.
   *
   * @return \Drupal\Core\Access\AccessResult
   *   The access result.
   */
  public function access(): AccessResult {
    $auth_key = $this->shipStationConfig->get('commerce_shipstation_alternate_auth');
    $username = $this->shipStationConfig->get('commerce_shipstation_username');
    $password = $this->shipStationConfig->get('commerce_shipstation_password');

    if ($this->request->query->get('SS-UserName') === $username && $this->request->query->get('SS-Password') === $password) {
      return AccessResult::allowed();
    }

    // Allow ShipStation to authenticate using an auth token.
    $request_auth_key = $this->request->query->get('auth_key');
    if ($auth_key && $request_auth_key && $auth_key === $request_auth_key) {
      return AccessResult::allowed();
    }

    // If all authentication methods fail, return a 401.
    $this->messenger->addError($this->t('Error: Authentication failed. Please check your credentials and try again.'));
    $this->logger->error('Error: Authentication failed when accepting request. Enable or check ShipStation request logging for more information.');
    throw new AccessDeniedHttpException("WWW-Authenticate: Basic realm =\"ShipStation XML API for Drupal Commerce");
  }

  /**
   * Identify orders to send back to shipstation.
   *
   * @param string $start_date
   *   The start date in UTC time.
   *   Format: MM/dd/yyyy HH:mm (24 hour notation).
   *   For example: 03/23/2012 21:09.
   * @param string $end_date
   *   The end date in UTC time. Same format as start_date.
   * @param int $page
   *   A current page. See "Paging" on the shipstation docs:
   *   https://help.shipstation.com/hc/en-us/articles/205928478#1a.
   *
   * @return string
   *   The XML formatted data.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function exportOrders(string $start_date, string $end_date, int $page = 1): string {
    $timezone_code = $this->systemDateConfig->get('timezone.default') ?: 'UTC';
    $timezone = new \DateTimeZone($timezone_code);
    $start = new \DateTime($start_date, $timezone);
    $end = new \DateTime($end_date, $timezone);
    $status = $this->shipStationConfig->get('commerce_shipstation_export_status');
    $page_size = $this->shipStationConfig->get('commerce_shipstation_export_paging');
    // @todo $shipping_services = commerce_shipping_services();
    $available_methods = $this->shipStationConfig->get('commerce_shipstation_exposed_shipping_methods');

    // Determine site-specific field reference fields.
    $field_billing_phone_number = $this->shipStationConfig->get('commerce_shipstation_billing_phone_number_field');
    $field_shipping_phone_number = $this->shipStationConfig->get('commerce_shipstation_shipping_phone_number_field');
    $field_order_notes = $this->shipStationConfig->get('commerce_shipstation_order_notes_field');
    $field_customer_notes = $this->shipStationConfig->get('commerce_shipstation_customer_notes_field');
    $field_product_images = $this->shipStationConfig->get('commerce_shipstation_product_images_field');

    // Build a query to load orders matching our status.
    $query = $this->entityTypeManager->getStorage('commerce_order')->getQuery();
    $query->condition('state', array_keys($status), 'IN');

    // Limit our query by start date and end date unless we're
    // doing a full reload.
    if (!$this->shipStationConfig->get('commerce_shipstation_reload')) {
      $query->condition('changed', [
        $start->getTimestamp(),
        $end->getTimestamp(),
      ], 'BETWEEN');
    }
    $query->accessCheck(FALSE);
    $query->pager($page_size);
    $results = $query->execute();
    $count_result = $query->count()->execute();
    $pages = ceil($count_result / $page_size);

    // Instantiate a new XML object for our export.
    $output = new ShipStationSimpleXMLElement('<Orders></Orders>');

    // Log the request information.
    if ($this->shipStationConfig->get('commerce_shipstation_logging')) {
      $request_info['Action'] = self::EXPORT_ACTION;
      $request_info['Orders'] = (isset($results) ? $count_result : 0);
      $request_info['Start'] = $this->dateFormatter->format($start->getTimestamp(), 'html_datetime') . ' (' . $start->getTimestamp() . ')';
      $request_info['End'] = $this->dateFormatter->format($end->getTimestamp(), 'html_datetime') . ' (' . $end->getTimestamp() . ')';
      $this->logger->info('@message', ['@message' => \json_encode($request_info)]);
    }

    if (isset($results)) {
      $orders = $this->entityTypeManager->getStorage('commerce_order')->loadMultiple(array_keys($results));
      // Allow other modules to alter the list of orders.
      $context = [
        'start_date' => $start->getTimestamp(),
        'end_date' => $end->getTimestamp(),
        'page' => $page,
        'page_size' => $page_size,
      ];
      $this->moduleHandler->alter('commerce_shipstation_export_orders', $orders, $context);

      $output['pages'] = $pages;

      /** @var \Drupal\commerce_order\Entity\OrderInterface $order */
      foreach ($orders as $order) {
        try {
          $profile = $order->getBillingProfile();
          /** @var \Drupal\address\Plugin\Field\FieldType\AddressItem $bill */
          $bill = $profile ? $profile->get('address')->first() : FALSE;
        }
        catch (\Exception $ex) {
          $bill = FALSE;
        }

        try {
          if ($order->hasField('shipments') || !$order->get('shipments')->isEmpty()) {
            $shipments = $order->get('shipments')->getValue();
            if ($shipments) {
              /** @var \Drupal\commerce_shipping\Entity\ShipmentInterface $shipment */
              $shipment = $this->entityTypeManager->getStorage('commerce_shipment')->load($shipments[0]['target_id']);
              $profile = $shipment->getShippingProfile();
              /** @var \Drupal\address\Plugin\Field\FieldType\AddressItem $ship */
              $ship = $profile ? $profile->get('address')->first() : FALSE;
            }
          }
        }
        catch (\Exception $ex) {
          $ship = FALSE;
        }

        if (!isset($ship) || !$ship) {
          continue;
        }

        if ($this->shipStationConfig->get('commerce_shipstation_logging')) {
          $this->logger->info('@message', ['@message' => 'Processing order ' . $order->id()]);
        }

        // Load the shipping line items.
        $shipping_items = $shipment->getItems();
        // Determine the shipping service and shipping method for the order.
        if (!empty($shipping_items)) {
          try {
            /** @var \Drupal\commerce_shipping\Entity\ShippingMethodInterface $shipping_method */
            $shipping_method = $shipment->getShippingMethod();
          }
          catch (\Exception $ex) {
            $shipping_method = FALSE;
          }
        }
        else {
          // Do not proceed if a shipping line item does not exist.
          continue;
        }

        // Only process orders which have authorized shipping methods.
        if (!empty($shipping_method) && in_array($shipping_method->getPlugin()->getPluginId(), $available_methods, TRUE)) {

          // Set up the xml schema.
          $order_xml = $output->addChild('Order');
          $order_date = $order->getPlacedTime();

          $order_fields = [
            '#cdata' => [
              'OrderNumber' => $order->getOrderNumber(),
              'OrderStatus' => $order->getState()->value,
              'ShippingMethod' => $shipping_method->getName(),
            ],
            '#other' => [
              'OrderDate' => date(self::DATE_FORMAT, $order_date),
              'LastModified' => date(self::DATE_FORMAT, $order->changed->value),
              'OrderTotal' => $order->getTotalPrice()->getNumber(),
              'ShippingAmount' => $shipment->getAmount()->getNumber(),
            ],
          ];

          if (strtolower($field_order_notes) !== 'none') {
            try {
              $field_array = explode('.', $field_order_notes);
              $field_name = end($field_array);
              $order_fields['#cdata']['InternalNotes'] = $order->$field_name->value;
            }
            catch (\Exception $ex) {
              // No action needed if there are no order notes.
            }
          }

          if (strtolower($field_customer_notes) !== 'none') {
            try {
              $field_array = explode('.', $field_customer_notes);
              $field_name = end($field_array);
              $order_fields['#cdata']['CustomerNotes'] = $shipment->getShippingProfile()->$field_name->value;
            }
            catch (\Exception $ex) {
              // No action needed if there are no customer notes.
            }
          }

          // Billing address.
          $customer = $order_xml->addChild('Customer');

          $customer_fields = [
            '#cdata' => [
              'CustomerCode' => $order->getEmail(),
            ],
          ];
          $this->addCdata($customer, $customer_fields);

          // Billing info.
          $billing = $customer->addChild('BillTo');
          $billing_fields = [
            '#cdata' => [
              'Name' => $bill ? $bill->getGivenName() . ' ' . $bill->getFamilyName() : '',
              'Company' => $bill ? $bill->getOrganization() : '',
              'Email' => $order->getEmail(),
            ],
          ];

          if (strtolower($field_billing_phone_number) !== 'none') {
            try {
              $field_array = explode('.', $field_billing_phone_number);
              $field_name = end($field_array);
              $billing_fields['#cdata']['Phone'] = $order->getBillingProfile()->$field_name->value;
            }
            catch (\Exception $ex) {
              // No action needed if phone can't be added.
            }
          }
          $this->addCdata($billing, $billing_fields);

          // Shipping info.
          $shipping = $customer->addChild('ShipTo');
          $shipping_fields = [
            '#cdata' => [
              'Name' => $ship->getGivenName() . ' ' . $ship->getFamilyName(),
              'Company' => $ship->getOrganization(),
              'Address1' => $ship->getAddressLine1(),
              'Address2' => $ship->getAddressLine2(),
              'City' => $ship->getLocality(),
              'State' => $ship->getAdministrativeArea(),
              'PostalCode' => $ship->getPostalCode(),
              'Country' => $ship->getCountryCode(),
            ],
          ];
          if (strtolower($field_shipping_phone_number) !== 'none') {
            try {
              $field_array = explode('.', $field_shipping_phone_number);
              $field_name = end($field_array);
              $shipping_fields['#cdata']['Phone'] = $shipment->getShippingProfile()->$field_name->value;
            }
            catch (\Exception $ex) {
              // No action necessary if phone can't be added.
            }
          }
          $this->addCdata($shipping, $shipping_fields);

          $line_items_xml = $order_xml->addChild('Items');

          foreach ($shipping_items as $shipping_item) {
            $weight_field = $shipping_item->getWeight();
            $order_item = OrderItem::load($shipping_item->getOrderItemId());
            if ($order_item === NULL) {
              continue;
            }
            /** @var \Drupal\commerce_product\Entity\ProductVariationInterface $product_variation */
            $product_variation = $order_item->getPurchasedEntity();
            if ($product_variation === NULL) {
              continue;
            }
            /** @var \Drupal\commerce_product\Entity\ProductInterface $product */
            $product = $product_variation->getProduct();

            $line_item_xml = $line_items_xml->addChild('Item');
            $line_item_cdata = [
              'SKU' => $product_variation->getSku(),
              'Name' => $shipping_item->getTitle(),
            ];

            if (strtolower($field_product_images) !== 'none') {
              try {
                $product_image = explode('.', $field_product_images);
                $image = $product->get(end($product_image));

                if ($image->getFieldDefinition()->getType() !== 'image') {
                  $image = FALSE;
                }
              }
              catch (\Exception $ex) {
                $image = FALSE;
              }

              if (!empty($image)) {
                if ($image->entity && $image->entity instanceof FileInterface) {
                  $url = $image->entity->getFileUri();
                  $url = Url::fromUri($url, ['absolute' => TRUE])->toUriString();
                  $line_item_cdata['ImageUrl'] = $this->entityTypeManager->getStorage('image_style')
                    ->load('thumbnail')
                    ->buildUrl($url);
                }
              }
            }

            $quantity = (int) $shipping_item->getQuantity();

            $line_item_fields = [
              '#cdata' => $line_item_cdata,
              '#other' => [
                'Quantity' => $quantity,
                'UnitPrice' => $order_item->getAdjustedUnitPrice()->getNumber(),
              ],
            ];

            // Add the line item weight.
            if (!empty($weight_field) && !empty($weight_field->getNumber())) {
              try {
                $this->addWeight($line_item_fields['#other'], (int) $weight_field->getNumber(), $weight_field->getUnit(), $quantity);
              }
              catch (\Exception $ex) {
                // The current item doesn't have a weight or we can't access it.
                if ($this->shipStationConfig->get('commerce_shipstation_logging')) {
                  $this->logger->warning('Unable to add weight for product id :product_id to shipstation export', [':product_id' => $product->id()]);
                }
              }
            }

            $this->addCdata($line_item_xml, $line_item_fields);
          }
          // Parse price component data for taxes and discounts.
          $commerce_adjustments = $order->collectAdjustments();
          /** @var \Drupal\commerce_order\Adjustment $adjustment */
          foreach ($commerce_adjustments as $adjustment) {

            // Skip shipping costs.
            if ($adjustment->getType() === 'shipping') {
              continue;
            }

            // @todo Test Tax
            // Append tax data to the response.
            if ($adjustment->getType() === 'tax') {
              if (!isset($order_fields['#cdata']['TaxAmount'])) {
                $order_fields['#cdata']['TaxAmount'] = 0;
              }
              $order_fields['#cdata']['TaxAmount'] += round($adjustment->getAmount()->getNumber(), 2);
            }

            // Create line items for promotions/discounts.
            if ($adjustment->getType() === 'promotion') {
              $line_item_xml = $line_items_xml->addChild('Item');

              $line_item_cdata = [
                'SKU' => NULL,
                'Name' => $adjustment->getLabel(),
              ];

              $line_item_fields = [
                '#cdata' => $line_item_cdata,
                '#other' => [
                  'Quantity' => 1,
                  'UnitPrice' => $adjustment->getAmount()->getNumber(),
                  'Adjustment' => TRUE,
                ],
              ];

              $this->addCdata($line_item_xml, $line_item_fields);
            }

          }

          $this->addCdata($order_xml, $order_fields);

          // Alter order XML.
          $this->moduleHandler->alter('commerce_shipstation_order_xml', $order_xml, $order);

          // Dispatch an event.
          $event = new ShipStationOrderExportedEvent($order);
          $this->eventDispatcher->dispatch($event, ShipStationEvents::ORDER_EXPORTED);
        }
      }
    }

    // Return the XML data for ShipStation.
    $dom = dom_import_simplexml($output)->ownerDocument;
    $dom->formatOutput = TRUE;
    return $dom->saveXML();
  }

  /**
   * Callback for ShipStation shipnotify requests.
   *
   * @param string|null $order_number
   *   The order number.
   * @param string|null $tracking_number
   *   The tracking number.
   * @param string|null $carrier
   *   The carrier.
   * @param string|null $ship_date_string
   *   The ship date string.
   *
   * @return \Drupal\Core\StringTranslation\TranslatableMarkup|null
   *   The response.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public function requestShipNotify(?string $order_number, ?string $tracking_number, ?string $carrier, ?string $ship_date_string): ?TranslatableMarkup {
    $timezone_code = $this->systemDateConfig->get('timezone.default') ?: 'UTC';
    $timezone = new \DateTimeZone($timezone_code);
    $ship_date = new \DateTime($ship_date_string, $timezone);

    // Order number and carrier are required fields for ShipStation and should
    // always be provided in a shipnotify call.
    if (($order_number !== NULL) && ($carrier !== NULL)) {
      /** @var \Drupal\commerce_order\Entity\OrderInterface $order */
      $orders = $this->entityTypeManager->getStorage('commerce_order')->loadByProperties(['order_number' => $order_number]);
      $order = array_pop($orders);

      if ($order !== NULL) {
        $shipments = $order->get('shipments')->getValue();
        /** @var \Drupal\commerce_shipping\Entity\ShipmentInterface $shipment */
        $shipment = $this->entityTypeManager->getStorage('commerce_shipment')->load($shipments[0]['target_id']);

        // Update shipping information.
        $shipment->setTrackingCode($tracking_number);
        $shipment->setShippedTime($ship_date->getTimestamp());
        // @todo Shipping method and Service.
        try {
          $shipment->save();
        }
        catch (EntityStorageException $e) {
          $this->logger->error('Shipping Information for Order: @order could not be updated.  Please try again later.', ['@order' => $order->id()]);
          throw new NotFoundHttpException();
        }

        // Update order.
        $state = $order->getState();
        if ($state->isTransitionAllowed('fulfill')) {
          $state->applyTransitionById('fulfill');
        }
        else {
          $this->logger->error(
            'ShipStation was unable to update status of order @order_number, no "fulfill" transition was found. Order type uses workflow @workflow.',
            [
              '@order_number' => $order->id(),
              '@workflow' => $state->getWorkflow()->getId(),
            ]
          );
        }
        $order->save();

        return $this->t('Tracking information was received successfully for order: @order', ['@order' => $order->id()]);
      }

      $this->logger->error('Unable to load order @order_number for updating via the ShipStation shipnotify call.', ['@order_number' => $order_number]);
      throw new NotFoundHttpException();
    }

    $this->logger->error('Required information (Order Number and Carrier) missing from ShipStation request.');
    throw new NotFoundHttpException();
  }

  /**
   * Helper function to add CDATA segments to XML file.
   *
   * @param \Drupal\commerce_shipstation\ShipStationSimpleXMLElement $xml
   *   The xml element.
   * @param array $data
   *   The data.
   */
  protected function addCdata(ShipStationSimpleXMLElement $xml, array $data): void {
    if (isset($data['#cdata'])) {
      foreach ($data['#cdata'] as $field_name => $value) {
        $xml->{$field_name} = NULL;
        $xml->{$field_name}->addCdata($value);
      }
    }
    if (isset($data['#other'])) {
      foreach ($data['#other'] as $field_name => $value) {
        $xml->{$field_name} = $value;
      }
    }
  }

  /**
   * Helper function to format product weight.
   *
   * @param array $data
   *   Array of weight data.
   * @param int $weight
   *   The weight of the item.
   * @param string $weight_units
   *   The unit code for the weight.
   * @param int $quantity
   *   The quantity for the line item.
   */
  protected function addWeight(array &$data, int $weight, string $weight_units, int $quantity): void {
    if ($quantity > 0) {
      $weight = $weight / $quantity;
    }
    switch ($weight_units) {
      case 'g':
        $weight_units = 'Gram';
        break;

      case 'lb':
        $weight_units = 'Pounds';
        break;

      case 'oz':
        $weight_units = 'Ounces';
        break;

      case 'kg':
        $weight_units = 'Gram';
        $weight = $weight * 1000;
        break;

      case 'mg':
        $weight_units = 'Gram';
        $weight = $weight / 1000;
        break;
    }
    $data['Weight'] = (string) $weight;
    $data['WeightUnits'] = $weight_units;
  }

}
