<?php

namespace Drupal\commerce_shipstation;

/**
 * Wrapper class to ease XML CDATA using.
 */
class ShipStationSimpleXMLElement extends \SimpleXMLElement {

  /**
   * Add CDATA segment.
   *
   * @param string $cdata_text
   *   The data.
   */
  public function addCdata($cdata_text): void {
    $node = dom_import_simplexml($this);
    $no = $node->ownerDocument;
    $node->appendChild($no->createCDATASection($cdata_text));
  }

}
