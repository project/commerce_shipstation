<?php

namespace Drupal\Tests\commerce_shipstation\Kernel\Access;

use Drupal\Tests\commerce_shipstation\Kernel\ShipStationKernelTestBase;

/**
 * @coversDefaultClass \Drupal\commerce_shipstation\Access\ShipStationAccess
 */
class ShipStationAccessTest extends ShipStationKernelTestBase {

  /**
   * @covers ::access
   */
  public function testAccess(): void {
    // @todo write test for ::access.
  }

}
