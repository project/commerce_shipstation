<?php

namespace Drupal\Tests\commerce_shipstation\Kernel\Controller;

use Drupal\Tests\commerce_shipstation\Kernel\ShipStationKernelTestBase;

/**
 * @coversDefaultClass \Drupal\commerce_shipstation\Controller\ShipStationEndpointController
 */
class ShipStationEndpointControllerTest extends ShipStationKernelTestBase {

  /**
   * @covers ::shipStationEndpointRequest.
   */
  public function testShipStationEndpointRequest(): void {
    // @todo write test for ::shipStationEndpointRequest.
  }

}
