<?php

namespace Drupal\Tests\commerce_shipstation\Kernel;

/**
 * Tests ShipStation.php.
 *
 * @coversDefaultClass \Drupal\commerce_shipstation\ShipStation
 */
class ShipStationTest extends ShipStationKernelTestBase {

  /**
   * @covers ::access
   */
  public function testAccess(): void {
    // @todo write test for ::access.
  }

  /**
   * @covers ::exportOrders
   */
  public function testExportOrders(): void {
    // @todo write test for ::exportOrders.
  }

  /**
   * @covers ::requestShipNotify
   */
  public function testRequestShipNotify(): void {
    // @todo write test for ::requestShipNotify.
  }

  /**
   * @covers ::addCdata
   */
  public function testAddCdata(): void {
    // @todo write test for ::addCdata.
  }

  /**
   * @covers ::addWeight
   */
  public function testAddWeight(): void {
    // @todo write test for ::addWeight.
  }

}
